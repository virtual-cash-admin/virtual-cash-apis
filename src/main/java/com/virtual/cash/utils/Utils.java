package com.virtual.cash.utils;

import com.virtual.cash.dao.CustomerDao;
import com.virtual.cash.model.Account;
import com.virtual.cash.model.Customer;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Utils {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public List<Customer> setup(CustomerDao customerDao) {
        List<Customer> customers = new ArrayList<>();
        for (int x=0;x<20;x++){
            List<Account> accounts  = new ArrayList<>();
            accounts.add(new Account("1"+x, "Super Saver", x+"100"));
            accounts.add(new Account("2"+x, "Current Account", x+"200"));
            accounts.add(new Account("3"+x, "Benefit Multiplier", x+"300"));
            accounts.add(new Account("4"+x, "Shopping Paradise", x+"400"));

            Customer customer = new Customer("userId"+x,
                    "User Name "+x,
                    accounts,
                    null,
                    null);
            customers.add(customer);
        }



        List<Account> jaggisbAccounts  = new ArrayList<>();
        jaggisbAccounts.add(new Account("511", "Super Saver", "3240"));
        jaggisbAccounts.add(new Account("512", "Current Account", "340"));
        jaggisbAccounts.add(new Account("513", "Benefit Multiplier", "1340"));
        jaggisbAccounts.add(new Account("514", "Shopping Paradise", "3340"));
        Customer jaggisb = new Customer("jaggisb",
                "Sandeep Jaggi",
                jaggisbAccounts,
                null,
                null);
        customers.add(jaggisb);

        List<Account> midSeanAccounts  = new ArrayList<>();
        midSeanAccounts.add(new Account("611", "Super Saver", "3240"));
        midSeanAccounts.add(new Account("612", "Current Account", "6000"));
        midSeanAccounts.add(new Account("613", "Benefit Multiplier", "2300"));
        midSeanAccounts.add(new Account("614", "Shopping Paradise Multiplier", "4300"));

        Customer midSean = new Customer("midSean",
                "Sean Middley",
                midSeanAccounts,
                null,
                null);
        customers.add(midSean);
        customerDao.insert(customers);





        return customers;
    }

    public Date getTodaysDate() {
        Date today = new Date();
        return today;
    }

    public  String getDateString(Date date) {
        return simpleDateFormat.format(date);
    }


    public static Date getDateFromString(String dateStr){
        try{
            return simpleDateFormat.parse(dateStr);
        }catch (Exception e){
            // Nothing to do i don't want this to break
        }
        return null;
    }


}
