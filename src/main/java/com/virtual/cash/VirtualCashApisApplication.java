package com.virtual.cash;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VirtualCashApisApplication {


	public static void main(String[] args) {
		SpringApplication.run(VirtualCashApisApplication.class, args);
	}

}
