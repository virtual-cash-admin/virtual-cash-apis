package com.virtual.cash.service;

import com.virtual.cash.dao.CustomerDao;
import com.virtual.cash.model.*;
import com.virtual.cash.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VirtualCashServiceImpl implements VirtualCashService {

    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private Utils utils;

    private Customer adjust(Customer customer) {
        if (customer.getActivePot() == null) {
            customer.setActivePot(new Pot("",
                    "",
                    "",
                    "",
                    false,
                    "",
                    new ArrayList<>(),
                    new ArrayList<>()));
        } else {
            if (!StringUtils.isEmpty(customer.getActivePot().getPotName())) {
                List<Piechart> pieChartData = pieChartData(customer.getActivePot());
                customer.getActivePot().setPieChartSpendings(pieChartData);
            } else {
                customer.getActivePot().setPieChartSpendings(new ArrayList<>());
            }
        }
        if (customer.getUsedPots() != null) {
            for (Pot userPot : customer.getUsedPots()) {
                userPot.setPieChartSpendings(pieChartData(userPot));
            }
        }
        return customer;
    }

    @Override
    public Customer getCustomer(String userId) {
        Customer customer = customerDao.findById(userId).get();
        Customer newCustomer = adjust(customer);
        return newCustomer;
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerDao.findAll();
    }


    @Override
    public Response createOrUpdatePot(String userId,
                                      String potName,
                                      String allocatedAmountStr,
                                      String expiryDate,
                                      String accountId) {
        Customer customer = adjust(customerDao.findById(userId).get());
        Account account =
                customer.getAccounts().stream().filter(account1 ->
                        account1.getAccountNumber().equalsIgnoreCase(accountId)).
                        collect(Collectors.toList()).get(0);
        Pot existingPot = customer.getActivePot();
        Integer allocatedAmount = Integer.valueOf(allocatedAmountStr);
        String currentBalanceStr = account.getBalance();
        Integer newBalance = Integer.valueOf(currentBalanceStr) -
                allocatedAmount + (existingPot.getAllocatedAmount().trim().equalsIgnoreCase("") ? 0 :
                Integer.valueOf(existingPot.getAllocatedAmount()));
        account.setBalance("" + newBalance);
        Integer totalAmountSpent = 0;
        if (existingPot.getSpendings() != null) {
            for (Spending spending : existingPot.getSpendings()) {
                Integer spentAmount = Integer.valueOf(spending.getAmountSpent());
                totalAmountSpent = totalAmountSpent + spentAmount;
            }
        }

        Pot pot = new Pot(potName, "" + allocatedAmount,
                "" + (allocatedAmount - totalAmountSpent), expiryDate,
                true, accountId, existingPot.getSpendings() != null ? existingPot.getSpendings() : new ArrayList<>(), null);


        customer.setActivePot(pot);
        customerDao.save(customer);
        return new Response("SUCCESS", "POT " + potName + " with balance " + allocatedAmount +
                " created successfully " + " Here are the details " + pot.toString());

    }


    @Override
    public String spend(String userId, String desc, String amountSpentStr, String tnxDateStr) {
        Customer customer = adjust(customerDao.findById(userId).get());
        Pot pot = customer.getActivePot();
        String remainingAmountStr = pot.getRemainingBalance();
        Integer amountSpent = Integer.valueOf(amountSpentStr);
        if (amountSpent > Integer.valueOf(remainingAmountStr)) {
            return "Your POT does not have sufficient money allocation. Please add money before spending.";
        } else {
            Integer newRemainingAmount = Integer.valueOf(remainingAmountStr) - amountSpent;
            pot.setRemainingBalance("" + newRemainingAmount);
            List<Spending> spendingList = pot.getSpendings() != null ? pot.getSpendings() : new ArrayList<>();
            Spending spending = new Spending(desc, "" + amountSpent, tnxDateStr);
            spendingList.add(spending);
            pot.setSpendings(spendingList);

            customer.setActivePot(pot);
            customerDao.save(customer);
            return "You have successully made following transaction " +
                    spending + " from your pot " + pot.getPotName() + " " +
                    "and your current balance in the pot is " + pot.getRemainingBalance();
        }
    }

    @Override
    public List<Alert> createAlerts(String userId) {
        List<Alert> alertList = new ArrayList<>();
        Customer customer = adjust(customerDao.findById(userId).get());
        Pot pot = customer.getActivePot();
        if (pot != null && pot.isActive()) {

            if (Integer.valueOf(pot.getRemainingBalance()) <= 10) {
                alertList.add(new Alert("remainingBalance", "Your pot balance is low."));
            }
            Date todayDate = utils.getTodaysDate();
            String todayStr  = utils.getDateString(todayDate);

            if (Utils.getDateFromString(todayStr).equals(Utils.getDateFromString(pot.getExpiryDate()))) {
                alertList.add(new Alert("expiryDate", "Your pot is expiring today."));
            }
        }
        return alertList;
    }

    @Override
    public Denominations denominations(String userID, String type) {
        Customer customer = adjust(customerDao.findById(userID).get());
        if (!customer.getActivePot().isActive()) {
            return new Denominations("0", "0",
                    "0", "0",
                    "0", "0",
                    "0", "0",
                    "0", "0");
        }

        String amount = "";
        if (type.equalsIgnoreCase("RB")) {
            amount = customer.getActivePot().getRemainingBalance();
        } else {
            amount = customer.getActivePot().getAllocatedAmount();
        }

        if (StringUtils.isEmpty(amount)) {
            return new Denominations("0", "0",
                    "0", "0",
                    "0", "0",
                    "0", "0",
                    "0", "0");
        }

        Integer n = Integer.valueOf(amount);
        int notes[] = {50, 20, 10, 5, 1};
        int noOf50P, noOf20P, noOf10P, noOf5P;
        noOf50P = n / 50;
        n = n % 50;
        noOf20P = n / 20;
        n = n % 20;
        noOf10P = n / 10;
        n = n % 10;
        noOf5P = n / 5;
        n = n % 5;


        return new Denominations("" + noOf50P, "" + (noOf50P * 50),
                "" + noOf20P, "" + (noOf20P * 20),
                "" + noOf10P, "" + (noOf10P * 10),
                "" + noOf5P, "" + (noOf5P * 5),
                "" + n, "" + (n * 1));
    }

    @Override
    public Response deactivatePot(String userId) {
        Customer customer = customerDao.findById(userId).get();
        Pot activePot = customer.getActivePot();
        Integer remainingBalance = Integer.valueOf(activePot.getRemainingBalance());
        String accountId = activePot.getAccountId();
        Account account = customer.getAccounts().stream().filter(account1 ->
                account1.getAccountNumber().equalsIgnoreCase(accountId)).
                collect(Collectors.toList()).get(0);
        Integer balance = Integer.valueOf(account.getBalance()) + remainingBalance;
        account.setBalance("" + balance);
        activePot.setActive(false);
        List<Spending> spendingList = activePot.getSpendings();
        if (spendingList == null) {
            spendingList = new ArrayList<>();
        }
        spendingList.add(new Spending("REVERTED",
                activePot.getRemainingBalance(),
                utils.getDateString(utils.getTodaysDate())));
        List<Pot> usedPots = customer.getUsedPots();
        if (usedPots == null) {
            usedPots = new ArrayList<>();
        }
        usedPots.add(activePot);
        customer.setUsedPots(usedPots);
        customer.setActivePot(new Pot("",
                "",
                "",
                "",
                false,
                "",
                new ArrayList<>(), new ArrayList<>()));
        customerDao.save(customer);
        return new Response("SUCCESS", activePot.toString() + " is deactivated and amount reverted back to account");
    }

    @Override
    public String setupData() {
        utils.setup(customerDao);
        return "Data Setup Completed";
    }

    @Override
    public String cleanData(String userId) {
        if ("ALL".equalsIgnoreCase(userId)) {
            customerDao.deleteAll();
            return "All customers deleted from database";
        } else {
            customerDao.deleteById(userId);
            return "User with user id " + userId + "  deleted from database";
        }
    }


    private List<Piechart> pieChartData(Pot pot) {
        List<Piechart> piecharts = new ArrayList<>();
        BigDecimal allocatedAmount = new BigDecimal(pot.getAllocatedAmount());
        BigDecimal totalSpending = new BigDecimal(0);
        if (pot.getSpendings() != null && pot.getSpendings().size() > 0) {
            for (Spending spending : pot.getSpendings()) {
                BigDecimal amountSpent = new BigDecimal(spending.getAmountSpent());
                totalSpending = totalSpending.add(amountSpent);
                BigDecimal share = amountSpent.divide(allocatedAmount, 7, RoundingMode.CEILING).multiply(new BigDecimal(100));
                Piechart piechart = new Piechart(share, spending.getDesc(),
                        " Spent £ " + amountSpent + " for " + spending.getDesc() + " on " + spending.getTnxDate());
                piecharts.add(piechart);
            }
        }
        if(pot.isActive()){
            BigDecimal remaingBalance = new BigDecimal(pot.getRemainingBalance());
            if (remaingBalance.doubleValue() > 0.0) {
                BigDecimal share = remaingBalance.divide(allocatedAmount, 7, RoundingMode.CEILING).multiply(new BigDecimal(100));
                Piechart piechart = new Piechart(share, "REMAINING",
                        "Amount left in your POT £ " + remaingBalance);
                piecharts.add(piechart);

            }
        }

        return piecharts;
    }


}
