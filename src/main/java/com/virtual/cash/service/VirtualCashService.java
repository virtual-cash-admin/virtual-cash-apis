package com.virtual.cash.service;

import com.virtual.cash.model.Alert;
import com.virtual.cash.model.Customer;
import com.virtual.cash.model.Denominations;
import com.virtual.cash.model.Response;

import java.util.List;

 public interface VirtualCashService {
     String setupData();
     String cleanData(String userId);

     Customer getCustomer(String userId);
     List<Customer> getAllCustomers();
     Response createOrUpdatePot(String userId,
                                String potName,
                                String allocatedAmount,
                                String expiryDate,
                                String accountId);

     Response deactivatePot(String userId);

     String spend(String userId,
                            String desc,
                            String amountSpent,
                            String tnxDate);

     List<Alert> createAlerts(String userId);

     Denominations denominations(String userId,String type);

}
