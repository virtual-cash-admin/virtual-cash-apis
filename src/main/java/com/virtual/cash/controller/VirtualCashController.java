package com.virtual.cash.controller;

import com.virtual.cash.model.Alert;
import com.virtual.cash.model.Customer;
import com.virtual.cash.model.Denominations;
import com.virtual.cash.model.Response;
import com.virtual.cash.service.VirtualCashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping(value = "/api/virtual-cash/")
public class VirtualCashController {

    @Autowired
    private VirtualCashService virtualCashService;

    @GetMapping(value = "/setupData")
    public String init() {
        return virtualCashService.setupData();
    }

    @CrossOrigin
    @GetMapping(value = "/get/{user_id}")
    public Customer getCustomer(@PathVariable(value = "user_id") String userId) {
        return virtualCashService.getCustomer(userId);
    }

    @GetMapping(value = "/getAll")
    @CrossOrigin
    public List<Customer> getAllCustomers() {
        return virtualCashService.getAllCustomers();
    }



    @GetMapping(value = "/createOrUpdatePot/{user_id}/{pot_name}/{allocated_amount}/{expiry_date}/{account_id}")
    @CrossOrigin
    public Response createOrUpdatePot(@PathVariable(value = "user_id") String userId,
                                      @PathVariable(value = "pot_name") String potName,
                                      @PathVariable(value = "allocated_amount") String allocatedAmount,
                                      @PathVariable(value = "expiry_date") String expiryDate,
                                      @PathVariable(value = "account_id") String accountId) {
        return virtualCashService.createOrUpdatePot(userId, potName, allocatedAmount, expiryDate, accountId);
    }

    @CrossOrigin
    @GetMapping(value = "/deactivePot/{user_id}")
    public Response deactivatePot(@PathVariable(value = "user_id") String userId){
        return virtualCashService.deactivatePot(userId);
    }

    @CrossOrigin
    @GetMapping(value = "/alerts/{user_id}")
    public List<Alert> alerts(@PathVariable(value = "user_id") String userId) {
        return virtualCashService.createAlerts(userId);
    }

    @CrossOrigin
    @GetMapping(value = "/spend/{user_id}/{description}/{amount_spent}/{tnx_date}")
    public String spend(@PathVariable(value = "user_id") String userId,
                        @PathVariable(value = "description") String description,
                        @PathVariable(value = "amount_spent") String amount_spent,
                        @PathVariable(value = "tnx_date") String tnx_date) {
        return virtualCashService.spend(userId, description, amount_spent, tnx_date);
    }

    @CrossOrigin
    @GetMapping(value = "/cleanData/{user_id}")
    public String cleanData(@PathVariable(value = "user_id") String userId) {
        return virtualCashService.cleanData(userId);
    }

    @CrossOrigin
    @GetMapping(value = "/denominations/{userId}/{type}")
    public Denominations denominations(@PathVariable(value = "userId") String userId,
                                       @PathVariable(value = "type") String type) {
        return virtualCashService.denominations(userId,type);
    }



}
