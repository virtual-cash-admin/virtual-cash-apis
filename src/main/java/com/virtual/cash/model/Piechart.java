package com.virtual.cash.model;

import java.math.BigDecimal;

public class Piechart {

    private BigDecimal share;
    private String name;
    private String information;

    public Piechart(BigDecimal share, String name, String information) {
        this.share = share;
        this.name = name;
        this.information = information;
    }

    public BigDecimal getShare() {
        return share;
    }

    public void setShare(BigDecimal share) {
        this.share = share;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    @Override
    public String toString() {
        return "Piechart{" +
                "share=" + share +
                ", name='" + name + '\'' +
                ", information='" + information + '\'' +
                '}';
    }
}
