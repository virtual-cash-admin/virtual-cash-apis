package com.virtual.cash.model;

public class Spending{
    private String desc;
    private String amountSpent;
    private String tnxDate;

    public Spending(String desc,
                    String amountSpent,
                    String tnxDate) {
        this.desc = desc;
        this.amountSpent = amountSpent;
        this.tnxDate = tnxDate;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAmountSpent() {
        return amountSpent;
    }

    public void setAmountSpent(String amountSpent) {
        this.amountSpent = amountSpent;
    }

    public String getTnxDate() {
        return tnxDate;
    }

    public void setTnxDate(String tnxDate) {
        this.tnxDate = tnxDate;
    }

    @Override
    public String toString() {
        return "Spending{" +
                "desc='" + desc + '\'' +
                ", amountSpent='" + amountSpent + '\'' +
                ", tnxDate='" + tnxDate + '\'' +
                '}';
    }

}
