package com.virtual.cash.model;

public class Alert {

    private String fieldName;
    private String alertMessage;

    @Override
    public String toString() {
        return "Alert{" +
                "fieldName='" + fieldName + '\'' +
                ", alertMessage='" + alertMessage + '\'' +
                '}';
    }

    public Alert(String fieldName, String alertMessage) {
        this.fieldName = fieldName;
        this.alertMessage = alertMessage;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }
}
