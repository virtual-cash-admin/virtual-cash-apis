package com.virtual.cash.model;

public class Denominations {

    private String noOf50P;
    private String tv50P;

    private String noOf20P;
    private String tv20P;

    private String noOf10P;
    private String tv10P;

    private String noOf5P;
    private String tv5P;

    private String noOf1P;
    private String tv1P;

    public Denominations(String noOf50P, String tv50P, String noOf20P, String tv20P, String noOf10P, String tv10P, String noOf5P, String tv5P, String noOf1P, String tv1P) {
        this.noOf50P = noOf50P;
        this.tv50P = tv50P;
        this.noOf20P = noOf20P;
        this.tv20P = tv20P;
        this.noOf10P = noOf10P;
        this.tv10P = tv10P;
        this.noOf5P = noOf5P;
        this.tv5P = tv5P;
        this.noOf1P = noOf1P;
        this.tv1P = tv1P;
    }

    public String getNoOf50P() {
        return noOf50P;
    }

    public void setNoOf50P(String noOf50P) {
        this.noOf50P = noOf50P;
    }

    public String getTv50P() {
        return tv50P;
    }

    public void setTv50P(String tv50P) {
        this.tv50P = tv50P;
    }

    public String getNoOf20P() {
        return noOf20P;
    }

    public void setNoOf20P(String noOf20P) {
        this.noOf20P = noOf20P;
    }

    public String getTv20P() {
        return tv20P;
    }

    public void setTv20P(String tv20P) {
        this.tv20P = tv20P;
    }

    public String getNoOf10P() {
        return noOf10P;
    }

    public void setNoOf10P(String noOf10P) {
        this.noOf10P = noOf10P;
    }

    public String getTv10P() {
        return tv10P;
    }

    public void setTv10P(String tv10P) {
        this.tv10P = tv10P;
    }

    public String getNoOf5P() {
        return noOf5P;
    }

    public void setNoOf5P(String noOf5P) {
        this.noOf5P = noOf5P;
    }

    public String getTv5P() {
        return tv5P;
    }

    public void setTv5P(String tv5P) {
        this.tv5P = tv5P;
    }

    public String getNoOf1P() {
        return noOf1P;
    }

    public void setNoOf1P(String noOf1P) {
        this.noOf1P = noOf1P;
    }

    public String getTv1P() {
        return tv1P;
    }

    public void setTv1P(String tv1P) {
        this.tv1P = tv1P;
    }
}
