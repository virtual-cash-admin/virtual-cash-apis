package com.virtual.cash.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "customer")
public class Customer {

    @Id
    private String userId;
    private String name;
    private List<Account> accounts;
    private List<Pot> usedPots;
    private Pot activePot;

    public Customer(String userId,
                    String name,
                    List<Account> accounts,
                    List<Pot> usedPots,
                    Pot activePot) {
        this.userId = userId;
        this.name = name;
        this.accounts = accounts;
        this.usedPots = usedPots;
        this.activePot = activePot;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<Pot> getUsedPots() {
        return usedPots;
    }

    public void setUsedPots(List<Pot> usedPots) {
        this.usedPots = usedPots;
    }

    public Pot getActivePot() {
        return activePot;
    }

    public void setActivePot(Pot activePot) {
        this.activePot = activePot;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", accounts=" + accounts +
                ", usedPots=" + usedPots +
                ", activePot=" + activePot +
                '}';
    }


}
