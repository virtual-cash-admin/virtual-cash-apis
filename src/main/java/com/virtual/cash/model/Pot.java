package com.virtual.cash.model;

import com.virtual.cash.utils.Utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Pot {
    private String potName;
    private String allocatedAmount;
    private String remainingBalance;
    private String expiryDate;
    private boolean active;
    private String accountId;
    private List<Spending> spendings;
    private List<Piechart> pieChartSpendings;


    public Pot(String potName,
               String allocatedAmount,
               String remainingBalance,
               String expiryDate,
               boolean active,
               String accountId,
               List<Spending> spendings,
               List<Piechart> pieChartSpendings) {
        this.potName = potName;
        this.allocatedAmount = allocatedAmount;
        this.remainingBalance = remainingBalance;
        this.expiryDate = expiryDate;
        this.active = active;
        this.accountId = accountId;
        this.spendings = spendings;
        this.pieChartSpendings = pieChartSpendings;
    }

    public String getPotName() {
        return potName;
    }

    public void setPotName(String potName) {
        this.potName = potName;
    }

    public String getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(String allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public String getRemainingBalance() {
        return remainingBalance;
    }

    public void setRemainingBalance(String remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setSpendings(List<Spending> spendings) {
        this.spendings = spendings;
    }

    public List<Piechart> getPieChartSpendings() {
        return pieChartSpendings;
    }

    public void setPieChartSpendings(List<Piechart> pieChartSpendings) {
        this.pieChartSpendings = pieChartSpendings;
    }

    public List<Spending> getSpendings() {
        if (spendings != null && spendings.size() > 0) {
            Collections.sort(spendings, new Comparator<Spending>() {
                @Override
                public int compare(Spending o1, Spending o2) {
                    Date date1 = Utils.getDateFromString(o1.getTnxDate());
                    Date date2 = Utils.getDateFromString(o2.getTnxDate());
                    if (date1 != null && date2 != null) {
                        return date1.compareTo(date2);
                    }
                    return 0;
                }
            });
        }
        return spendings;

    }


}


